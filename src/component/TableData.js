
import React ,{Component} from 'react';

class TableData extends Component {
    
    render () {
        console.log(this.props.filteredData);
        return (              
            <table className="table table-hover">
            <thead>
             <tr className="sizetr">
                 <th className="sizetr_col-id">ID</th>
                   <th className="sizetr_col-type"> type</th>
                   <th className="sizetr_col-file">File</th>
                   <th className="sizetr_col-project">Project</th>
                   <th className="sizetr_col-date">Date</th>
                   <th className="sizetr_col-size">Size</th>
               </tr>
           </thead>
           <tbody>
               {
                   this.props.filteredData.map((row,index) => (
                        <tr key={index}>
                            <td>{row.id}</td>
                            <td>{row.name}</td>
                            <td>Du lieu</td>
                            <td>{row.project}</td>
                            <td>{row.created_date}</td>
                            <td>{row.size}</td>
                        </tr>
                   ))
                }

           </tbody>

         </table>

    );
}
}
export default TableData;





