import React ,{Component}from 'react';
import './App.css';
import DataUser from './Data.json';
import TableData from './component/TableData';

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
        data : DataUser,
        term : ''
    }
      this.searchHandler = this.searchHandler.bind(this)
  }

  searchHandler (event) {
    this.setState ({ term : event.target.value })
    console.log (event.target.value)
  }

      render (){
        let filteredData = this.state.data.filter((value) => {
          return value.name.toLowerCase().includes(this.state.term.toLowerCase())
        })
        return (
          <div className="row-12">
            <div className="col-xl-12 col-md-12 ">     
            <div className="card ">
              <div className ="card-header "> 
                <h5 className="mb-0 card-title">Document
                  <div className="float-right col-sm-2">
                  <input type="text" className="form-control" placeholder="Search.." onChange={this.searchHandler}/>
                  </div>
                </h5>  
              </div>
              <div className="card-body">
              <div className="table-responsive">
                 <TableData filteredData = {filteredData}/>
             </div>
              </div>
            </div>
            </div>
          </div>

          
            




       );
    }
}

export default App;
